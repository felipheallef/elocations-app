# eLocations

Projeto de desafio para a vaga de Desenvolvedor Kotlin na Engeselt Softwares.

## Desafio 

Nesta etapa você deverá criar um app mobile em Kotlin chamado eLocations. Com o eLocations o usuário poderá cadastrar diversos tipos de estabelecimento com titulo, descrição, categoria, ponto geográfico e imagens e salvar em um BD local, o app também deverá ser capaz de editar e excluir os estabelecimentos e imagens.


![Protótipo](https://raw.githubusercontent.com/felipheallef/elocations-app/main/images/prototipo.jpeg)

## Solução

![Solução](https://raw.githubusercontent.com/felipheallef/elocations-app/main/images/solucao.jpg)